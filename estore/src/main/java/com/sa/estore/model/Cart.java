package com.sa.estore.model;

import com.sa.estore.dto.cart.AddToCartDto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private @NotBlank Integer userId;

    @Column(name = "product_id")
    private @NotBlank Long productId;

    @Column(name = "created_date")
    private Date createdDate;

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Product product;

    private int quantity;

    public Cart(AddToCartDto addToCartDto, int userId) {
        this.userId = userId;
        this.productId = addToCartDto.getProductId();
        this.quantity = addToCartDto.getQuantity();
        this.createdDate = new Date();
    }
}
