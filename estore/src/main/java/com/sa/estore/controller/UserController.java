package com.sa.estore.controller;

import com.sa.estore.dto.*;
import com.sa.estore.dto.user.SigninDto;
import com.sa.estore.dto.user.SigninResponseDto;
import com.sa.estore.dto.user.SignupDto;
import com.sa.estore.exceptions.AuthenticationFailException;
import com.sa.estore.exceptions.CustomException;
import com.sa.estore.model.User;
import com.sa.estore.repository.UserRepository;
import com.sa.estore.service.AuthenticationService;
import com.sa.estore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    UserService userService;

    @GetMapping("/all")
    public List<User> findAllUser(@RequestParam("token") String token) throws AuthenticationFailException {
        authenticationService.authenticate(token);
        return userRepository.findAll();
    }

    @PostMapping("/signup")
    public ResponseDto Signup(@RequestBody SignupDto signupDto) throws CustomException {
        return userService.signUp(signupDto);
    }

    @PostMapping("/signIn")
    public SigninResponseDto Signup(@RequestBody SigninDto signInDto) throws CustomException {
        return userService.signIn(signInDto);
    }

    // @PostMapping("/updateUser")
    // public ResponseDto updateUser(@RequestParam("token") String token,
    // @RequestBody UserUpdateDto userUpdateDto) {
    // authenticationService.authenticate(token);
    // return userService.updateUser(token, userUpdateDto);
    // }

    // @PostMapping("/createUser")
    // public ResponseDto updateUser(@RequestParam("token") String token,
    // @RequestBody UserCreateDto userCreateDto)
    // throws CustomException, AuthenticationFailException {
    // authenticationService.authenticate(token);
    // return userService.createUser(token, userCreateDto);
    // }
}
