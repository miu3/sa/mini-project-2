package com.sa.estore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sa.estore.model.Category;

@Repository
public interface Categoryrepository extends JpaRepository<Category, Long> {

	Category findByCategoryName(String categoryName);

}
