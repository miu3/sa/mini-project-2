package com.sa.estore.common;

import java.util.List;

import lombok.Data;

@Data
public class PagedList<T> {
	private int totalPages;
	private long totalElements;
	private boolean hasNext;
	private boolean hasPrevious;

	private List<T> data;
}
