package com.sa.estore.enums;

public enum Role {
    user,
    manager,
    admin
}
