package com.sa.estore.dto.cart;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartDto {
    private List<CartItemDto> cartItems;
    private double totalCost;
}
