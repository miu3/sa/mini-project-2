package com.sa.estore.dto.order;

import com.sa.estore.model.Order;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class PlaceOrderDto {
    private Integer id;
    private @NotNull Integer userId;
    private @NotNull Double totalPrice;

    public PlaceOrderDto(Order order) {
        this.setId(order.getId());
        this.setUserId(order.getUserId());
        this.setTotalPrice(order.getTotalPrice());
    }
}
