package com.sa.estore.dto.user;

import com.sa.estore.enums.Role;

import lombok.Data;

@Data
public class UserCreateDto {

    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private String password;

}
