package com.sa.estore.dto.order;

import com.sa.estore.model.Order;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OrderDto {
    private Integer id;
    private @NotNull Integer userId;

    public OrderDto(Order order) {
        this.setId(order.getId());
        this.setUserId(order.getUserId());
    }
}
