package com.sa.estore.dto.user;

import lombok.Data;

@Data
public class SigninDto {
    private String email;
    private String password;
}
