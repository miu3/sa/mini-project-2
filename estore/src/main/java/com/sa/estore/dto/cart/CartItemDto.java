package com.sa.estore.dto.cart;

import com.sa.estore.model.Cart;
import com.sa.estore.model.Product;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CartItemDto {
    private Integer id;
    private @NotNull Integer userId;
    private @NotNull Integer quantity;
    private @NotNull Product product;

    public CartItemDto(Cart cart) {
        this.setId(cart.getId());
        this.setUserId(cart.getUserId());
        this.setQuantity(cart.getQuantity());
        this.setProduct(cart.getProduct());
    }
}
