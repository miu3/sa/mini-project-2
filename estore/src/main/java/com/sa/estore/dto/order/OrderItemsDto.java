package com.sa.estore.dto.order;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class OrderItemsDto {

    private @NotNull double price;
    private @NotNull int quantity;
    private @NotNull int orderId;
    private @NotNull int productId;

}
