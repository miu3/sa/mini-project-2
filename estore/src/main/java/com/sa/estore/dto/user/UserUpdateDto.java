package com.sa.estore.dto.user;

import com.sa.estore.enums.Role;

import lombok.Data;

@Data
public class UserUpdateDto {
    // skipping updating passord as of now
    private Integer id;
    private String firstName;
    private String lastName;
    private Role role;
}
