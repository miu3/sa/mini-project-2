package com.sa.estore;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@RestController
public class eStoreApplication {

	@Value("${msg}")
	private String message;

	@GetMapping("/")
	public String index() {
		return "Hello World! " + message;
	}

	public static void main(String[] args) {
		new SpringApplicationBuilder(eStoreApplication.class)
				.run(args);
	}

}
